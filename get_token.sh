http://blog.keycloak.org/2015/10/getting-started-with-keycloak-securing.html

RESULT=`curl --data "grant_type=password&client_id=app-springboot&username=alice@keycloak.org&password=password" http://localhost:8080/auth/realms/quickstart/protocol/openid-connect/token`
TOKEN=`echo $RESULT | sed 's/.*access_token":"//g' | sed 's/".*//g'`

curl http://localhost:8081/products -H "Authorization: bearer $TOKEN"