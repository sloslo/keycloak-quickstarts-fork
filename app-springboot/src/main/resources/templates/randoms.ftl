<#import "/spring.ftl" as spring />
<#assign xhtmlCompliant = true in spring>
<!DOCTYPE html>
<html>
<head>
    <title>Randomsy</title>
</head>
<body>

<header>
     <a href="/logout" id="logout">Logout</a>
</header>

<h1>Randomsy</h1>
<p>User ${principal.name} made this request.</p>

<h2>Randomy</h2>
<ul>
    <#list randoms as random>
        <li>${random}</li>
    </#list>
</ul>

</body>
</html>
