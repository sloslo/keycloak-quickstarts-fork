package org.keycloak.quickstart.springboot.service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

@Component
public class RandomService {

	public List<String> generateSomeRandoms(final String pName) {
		return Arrays.asList(pName, UUID.randomUUID().toString(), UUID.randomUUID().toString(),
				UUID.randomUUID().toString());
	}
}
